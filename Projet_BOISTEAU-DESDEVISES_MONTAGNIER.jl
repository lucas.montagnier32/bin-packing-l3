# Erwan Boisteau-Desdevises - Lucas Montagnier, 685

using JuMP, GLPK # On va utiliser JuMP et GLPK


# Structures contenant les données d'un problème de bin-packing mono-dimensionnel
struct objet1D
	taille::Int64 # Taille de l'objet
	nb::Int64 # Nombre d'objets de la même taille
end

struct donnees1D
	T::Int64 # Taille d'un bin
	nb::Int64 # Nombre de tailles d'objet différents
	tab::Vector{objet1D} # Tableau des objets à insérer dans les bins
end


# Structures contenant les données d'un problème de bin-packing bi-dimensionnel
struct objet2D
	l::Int64 # Largeur de la pièce
	h::Int64 # Hauteur de la pièce
	nb::Int64 # Nombre d'objets dans ce format
end

struct donnees2D
	L::Int64 # Largeur d'un bin
	H::Int64 # Hauteur d'un bin
	nb::Int64 # Nombre de types d'objet différents
	tab::Vector{objet2D} # Tableau des objets à insérer dans les bins
end



# Fonction prenant en entrée un nom de fichier et retournant les données d'une instance de problème de bin-packing mono-dimensionnel
function parser_data1D(nomFichier::String)
    # Ouverture d'un fichier en lecture
    f = open(nomFichier,"r")

    # Lecture de la première ligne pour connaître la taille d'un bin et le nombre de tailles différentes pour les objets à ranger
    s::String = readline(f) # lecture d'une ligne et stockage dans une chaîne de caractères
    ligne::Vector{Int64} = parse.(Int64,split(s," ",keepempty = false)) # Segmentation de la ligne en plusieurs entiers, à stocker dans un tableau
    T::Int64 = ligne[1]
    nb::Int64 = ligne[2]

    # Allocation mémoire pour le tableau des objets
    tab = Vector{objet1D}(undef,nb)

    # Lecture des infos sur les objets (sur chaque ligne : taille + nombre)
    for i in 1:nb
        s = readline(f)
        ligne = parse.(Int64,split(s," ",keepempty = false))
        tab[i] = objet1D(ligne[1],ligne[2])
    end

    # Fermeture du fichier
    close(f)

    # Retour des donnees
    return donnees1D(T,nb,tab)
end


# Fonction prenant en entrée un nom de fichier et retournant les données d'une instance de problème de bin-packing bi-dimensionnel
function parser_data2D(nomFichier::String)
    # Ouverture d'un fichier en lecture
    f = open(nomFichier,"r")

    # Lecture de la première ligne pour connaître la largeur et la hauteur d'un bin et le nombre de types d'objets différents à ranger
    s::String = readline(f) # lecture d'une ligne et stockage dans une chaîne de caractères
    ligne::Vector{Int64} = parse.(Int64,split(s," ",keepempty = false)) # Segmentation de la ligne en plusieurs entiers, à stocker dans un tableau
	 L::Int64 = ligne[1]
	 H::Int64 = ligne[2]
	 nb::Int64 = ligne[3]

    # Allocation mémoire pour le tableau des objets
    tab = Vector{objet2D}(undef,nb)

    # Lecture des infos sur les objets (sur chaque ligne : largeur + hauteur + nombre)
    for i in 1:nb
        s = readline(f)
        ligne = parse.(Int64,split(s," ",keepempty = false))
        tab[i] = objet2D(ligne[1],ligne[2],ligne[3])
    end

    # Fermeture du fichier
    close(f)

    # Retour des donnees
    return donnees2D(L,H,nb,tab)
end


# Exemple de script (à adapter) pour résoudre des séries d'instances
#=
function scriptMonoA()
	d::donnees1D = parser_data1D("Instances/1Dim/A/A4.dat")
	maFonctionQuiResoutTropBienLeCasMonoDimensionnel(d) # Première résolution à part pour que le code soit compilé

	indices::Vector{Int64} = [4,5,6,7,8,9,10,15,20]
	for i in indices
		d = parser_data1D("Instances/1Dim/A/A$i.dat")
		@time maFonctionQuiResoutTropBienLeCasMonoDimensionnel(d)
	end
end
=#

# A vous de faire le reste...
#------------------------------------------------------------------
# 2.1
function maFonctionQuiResoutTropBienLHeuristiqueBestFit(d::donnees1D)
	
	nbObjets::Int64 = sum(d.tab[i].nb for i in 1:length(d.tab))
	println("Il y a ",nbObjets," objets")
	instance::donnees1D = deepcopy(d)
	taille_max_bin = instance.T
	println("La taille max d'un bin: ",taille_max_bin)
	
	# tri par ordre décroissant des objets en fonction de leur taille
	sort!(instance.tab, by = x -> x.taille, rev=true)
	println("Les objets a placés: ",instance.tab)

	nb_bins::Int64 = 1
	current_bin_place_occupe = 0

	old::Tuple{Int64,Int64} = (current_bin_place_occupe, nb_bins)
	# old correspondant a la place occupe dans le bin qu'on est en traint de remplir et nb_bins est le nombre total de bins utilisés
	for i in 1:length(instance.tab)
		old = MettreTousLesObjetsDeMemeTaille(instance, old[1], instance.tab[i], taille_max_bin)
		nb_bins += old[2]
	end
	
	println("Résultat de l'heuristique: ",nb_bins," bins utilisés (borne sup)")
	return nb_bins
end

function MettreTousLesObjetsDeMemeTaille(d::donnees1D, old_bin_place_occupe::Int64, o::objet1D, taille_max_bin::Int64)
	nb_bins::Int64 = 0
	current_bin_place_occupe = old_bin_place_occupe
	
	nb_objet_restant::Int64 = o.nb

	while nb_objet_restant>0
		if ((current_bin_place_occupe + o.taille) <= taille_max_bin)
			current_bin_place_occupe += o.taille
		else # nouveau bin
			nb_bins += 1
			current_bin_place_occupe = o.taille
		end
		nb_objet_restant -= 1
	end
	return (current_bin_place_occupe, nb_bins)
end


function scriptMono21()
	d::donnees1D = parser_data1D("Instances/1Dim/A/A4.dat")
	maFonctionQuiResoutTropBienLHeuristiqueBestFit(d) # Première résolution à part pour que le code soit compilé

	indices::Vector{Int64} = [4,5,6,7,8,9,10,15,20]
	for i in indices
		println("instance ",i)
		d = parser_data1D("Instances/1Dim/B/B$i.dat")
		@time maFonctionQuiResoutTropBienLHeuristiqueBestFit(d)
		println()
	end
end

#-----------------------------------------------------------------------------------------------------
# 2.2
function modelisationDirecteMono(solverSelected::DataType, nbObj::Int64, borneSupNbBin::Int64, taillesObjets::Vector{Int64}, tailleBin::Int64)
	# Déclartion du modèle
	m::Model = Model(solverSelected)
	set_optimizer_attribute(m, "msg_lev", GLPK.GLP_MSG_ALL)
	
	# Déclaration des variables
	# 1 si l'objet i est rangé dans le bin j
	@variable(m, x[1:nbObj,1:borneSupNbBin], binary = true)
	# 1 s'il y a un objet rangé dans le bin j
	@variable(m, y[1:borneSupNbBin], binary = true)
	
	# Déclaration de la fonction objectif
	@objective(m, Min, sum(y[j] for j in 1:borneSupNbBin))
	
	# Déclaration des contraintes
	# chaque objet doit être dans un bin
	@constraint(m, objetDansBin[i in 1:nbObj],sum(x[i,j] for j in 1:borneSupNbBin) == 1)
	# la somme des tailles des objets rangés dans un bin ne peut être supérieure à la taille du bin
	@constraint(m, bienRanges[j in 1:borneSupNbBin], sum(taillesObjets[i]*x[i,j] for i in 1:nbObj) <= tailleBin*y[j])
	
	# Valeur retournée
	return m
end

function maFonctionQuiResoutTropBienLaModelisationDirecte(d::donnees1D)
	# Déclaration des données
	# calcul du nombre d'objet total
	nbObjets::Int64 = sum(d.tab[i].nb for i in 1:length(d.tab))
	# calcul de la born sup sur le nombre de bins
	borneSupNbBin::Int64 = maFonctionQuiResoutTropBienLHeuristiqueBestFit(d)
	# Création du tableau contenant les tailles de chaques objets
	taillesDesObjets::Vector{Int64} = calculDesTaillesDesObjets(d, nbObjets)
	# Taille des bins
	tailleDesBin::Int64 = d.T
	
	# Création d'un modèle complété à partir des données
	m::Model = modelisationDirecteMono(GLPK.Optimizer, nbObjets, borneSupNbBin, taillesDesObjets, tailleDesBin)
	
	#print(m)
	
	# Résolution
	optimize!(m)
	
	# Affichage des résultats (ici assez complet pour gérer certains cas d'"erreur")
	status = termination_status(m)

	if status == MOI.OPTIMAL
		println("Problème résolu à l'optimalité")

		println("z = ",objective_value(m)) # affichage de la valeur optimale
		println("x = ",value.(m[:x])) # affichage des valeurs du vecteur de variables issues du modèle

		for i in 1:nbObjets
			for j in 1:borneSupNbBin
				if isapprox(value(m[:x][i,j]),1)
					println("objet ",i," dans bin ",j)
				end
			end
		end
		println()

	elseif status == MOI.INFEASIBLE
		println("Problème non-borné")

	elseif status == MOI.INFEASIBLE_OR_UNBOUNDED
		println("Problème impossible")
	end
end

function calculDesTaillesDesObjets(d::donnees1D, nbObj::Int64)
	println(nbObj," objets au total")
	# Création du tableau contenant les tailles de chaques objets
	lesTailles = Vector{Int64}(undef,nbObj)
	currentTypeIndice::Int64 = 1
	nbObjRestantUnType::Int64 = d.tab[currentTypeIndice].nb
	
	for i in 1:nbObj
		if (nbObjRestantUnType<=0)# si un on a rempli le tableau pour un type
			currentTypeIndice += 1
			nbObjRestantUnType = d.tab[currentTypeIndice].nb
		end
		lesTailles[i] = d.tab[currentTypeIndice].taille
		nbObjRestantUnType -= 1
	end
	sort!(lesTailles, rev=true)
	return lesTailles
end


function scriptMono22()
	#d::donnees1D = parser_data1D("Instances/1Dim/A/A4.dat")
	d::donnees1D = parser_data1D("Instances/1Dim/A/jouet.dat")

	maFonctionQuiResoutTropBienLaModelisationDirecte(d) # Première résolution à part pour que le code soit compilé
#=
	indices::Vector{Int64} = [4,5,6,7,8,9,10,15,20]
	for i in indices
		d = parser_data1D("Instances/1Dim/A/A$i.dat")
		@time maFonctionQuiResoutTropBienLaModelisationDirecte(d)
		println()
	end=#
end

#-----------------------------------------------------------------------------------------------------
# 2.2 alternatif
function modelisationDirecteMonoAlternatif(solverSelected::DataType, borneSupNbBin::Int64, tailleBin::Int64, nbObjDistincts::Int64, tailleObjetsDistinct::Vector{Int64}, nbObjType::Vector{Int64})
	# Déclartion du modèle
	m::Model = Model(solverSelected)
	set_optimizer_attribute(m, "msg_lev", GLPK.GLP_MSG_ALL)
	
	# Déclaration des variables
	# nombre d'objet de type i dans le bin j
	@variable(m, x[1:nbObjDistincts,1:borneSupNbBin] >= 0, integer = true)
	# 1 s'il y a un objet rangé dans le bin j
	@variable(m, y[1:borneSupNbBin], binary = true)
	
	# Déclaration de la fonction objectif
	@objective(m, Min, sum(y[j] for j in 1:borneSupNbBin))
	
	# Déclaration des contraintes
	# la somme des tailles des objets rangés dans un bin ne peut être supérieure à la taille du bin
	@constraint(m, bienRanges[j in 1:borneSupNbBin], sum(x[i,j]*tailleObjetsDistinct[i] for i in 1:nbObjDistincts) <= tailleBin*y[j])
	# le nombre d'objet de chaque type ne doit pas être dépassé
	@constraint(m, utiliseToutLesObjet[i in 1:nbObjDistincts], sum(x[i,j] for j in 1:borneSupNbBin) == nbObjType[i])
	
	# Valeur retournée
	return m
end

function maFonctionQuiResoutTropBienLaModelisationDirecteAlternatif(d::donnees1D)
	# Déclaration des données
	# calcul de la born sup sur le nombre de bins
	borneSupNbBin::Int64 = maFonctionQuiResoutTropBienLHeuristiqueBestFit(d)
	# Taille des bins
	tailleDesBin::Int64 = d.T
	# tableau contenant les valeurs distinct des tailles des objets
	tailleObjetsDistinct::Vector{Int64} = [d.tab[i].taille for i in 1:length(d.tab)]
	# nombre d'objet de tailles différente
	nbObjDistincts::Int64 = d.nb
	# le nombre d'objet par type
	nbObjType::Vector{Int64} = [d.tab[i].nb for i in 1:length(d.tab)]
	
	# Création d'un modèle complété à partir des données
	m::Model = modelisationDirecteMonoAlternatif(GLPK.Optimizer, borneSupNbBin, tailleDesBin, nbObjDistincts, tailleObjetsDistinct, nbObjType)
	
	print(m)
	
	# Résolution
	optimize!(m)
	
	# Affichage des résultats (ici assez complet pour gérer certains cas d'"erreur")
	status = termination_status(m)

	if status == MOI.OPTIMAL
		println("Problème résolu à l'optimalité")

		println("z = ",objective_value(m)) # affichage de la valeur optimale
		println("x = ",value.(m[:x])) # affichage des valeurs du vecteur de variables issues du modèle

		for i in 1:nbObjDistincts
			for j in 1:borneSupNbBin
				println("nombre d'objet de type ",i," dans bin ",j," = ",value(m[:x][i,j]))
			end
		end
		println()

	elseif status == MOI.INFEASIBLE
		println("Problème non-borné")

	elseif status == MOI.INFEASIBLE_OR_UNBOUNDED
		println("Problème impossible")
	end
end

function scriptMono22alternatif()
	d::donnees1D = parser_data1D("Instances/1Dim/A/A4.dat")
	#d::donnees1D = parser_data1D("Instances/1Dim/A/jouet.dat")

	maFonctionQuiResoutTropBienLaModelisationDirecteAlternatif(d) # Première résolution à part pour que le code soit compilé
#=
	indices::Vector{Int64} = [4,5,6,7,8,9,10,15,20]
	for i in indices
		d = parser_data1D("Instances/1Dim/A/A$i.dat")
		@time maFonctionQuiResoutTropBienLaModelisationDirecte(d)
		println()
	end=#
end

#-----------------------------------------------------------------------------------------------------
# 2.3
function modelisationIndirecteMono(solverSelected::DataType, Motifs::Vector{Vector{Int64}}, nbObjDistincts::Int64, nbObjType::Vector{Int64})
    nbMotifs::Int64 = length(Motifs)
    
	# Déclaration du modèle
	m::Model = Model(solverSelected)
	
	# Déclaration des variables
	# nombre de bins correspondant au motif j
	@variable(m, x[1:nbMotifs] >= 0, integer = true)
    
	# Déclaration de la fonction objectif
	@objective(m, Min, sum(x[j] for j in 1:nbMotifs))
	
	# Déclaration des contraintes
	@constraint(m, cont[i=1:nbObjDistincts], sum(compteObjetIDansMotif(i,Motifs[j])*x[j] for j in 1:nbMotifs) >= nbObjType[i])
	
	# Valeur retournée
	return m
end

function maFonctionQuiResoutTropBienLaModelisationIndirecte(d::donnees1D)
	# Déclaration des données
	objetsAux::Vector{objet1D} = sort!(deepcopy(d.tab), by = x -> x.taille, rev=true)
	
	# calcul du nombre d'objet total
	nbObjets::Int64 = sum(d.tab[i].nb for i in 1:length(d.tab))
	# calcul de la born sup sur le nombre de bins
	borneSupNbBin::Int64 = maFonctionQuiResoutTropBienLHeuristiqueBestFit(d)
	# Création du tableau contenant les tailles de chaques objets
	taillesDesObjets::Vector{Int64} = calculDesTaillesDesObjets(d, nbObjets)
	# Taille des bins
	tailleDesBin::Int64 = d.T
	# tableau contenant les valeurs distinct des tailles des objets
	tailleObjetsDistinct::Vector{Int64} = [objetsAux[i].taille for i in 1:length(d.tab)]
	# nombre d'objet de tailles différente
	nbObjDistincts::Int64 = d.nb
	# le nombre d'objet par type triée par taille des objets
	nbObjType::Vector{Int64} = [objetsAux[j].nb for j in 1:length(d.tab)]
	
	println("Nombre d'objet par type: ",nbObjType);
	# listes des motifs possible
	lesMotifs = donneLesMotifs(nbObjDistincts, tailleObjetsDistinct, tailleDesBin)
	println("Il y a ",length(lesMotifs)," motifs")
	#println("Resultat des motifs: ", lesMotifs)
	
	# Création d'un modèle complété à partir des données
	m::Model = modelisationIndirecteMono(GLPK.Optimizer, lesMotifs, nbObjDistincts, deepcopy(nbObjType))
	
	#print(m)
	
	# Résolution
	optimize!(m)
	
	# Affichage des résultats (ici assez complet pour gérer certains cas d'"erreur")
	status = termination_status(m)

	if status == MOI.OPTIMAL
		println("Problème résolu à l'optimalité")

		println("z = ",objective_value(m)) # affichage de la valeur optimale
		#println("x = ",value.(m[:x])) # affichage des valeurs du vecteur de variables issues du modèle


		for j in 1:length(lesMotifs)
			if (value(m[:x][j])!=0)
				println("nombre de bin correspondant au motif ",j,":",lesMotifs[j]," = ",value(m[:x][j]))
			end
		end
		println()

	elseif status == MOI.INFEASIBLE
		println("Problème non-borné")

	elseif status == MOI.INFEASIBLE_OR_UNBOUNDED
		println("Problème impossible")
	end
end

# lance la récursion en ajoutant un objet de chaques types par appelle récursif et récolte les motifs résultants
function donneLesMotifs(nbObjDistincts::Int64, tailleObjetsDistinct::Vector{Int64}, tailleDesBin::Int64)
	motifs::Vector{Vector{Int64}} = []
	for i in 1:nbObjDistincts
		append!(motifs, calculDesMotifsRec([i], tailleDesBin-tailleObjetsDistinct[i], tailleObjetsDistinct))
	end
	return motifs
end

function calculDesMotifsRec(objetsPrecedantsMis::Vector{Int64}, placeLibre::Int64, taillesDesObjets::Vector{Int64})
	# détermine si on peut encore ajouter un objet, le premier paramètre correspond au dernier élément ajouter a la liste des objets placés
	objetsQuOnPeutRajouter = onPeutAjouterQuoiCommeObjet(objetsPrecedantsMis[length(objetsPrecedantsMis)], placeLibre, taillesDesObjets)
	
	motifs::Vector{Vector{Int64}} = []
	
	if (length(objetsQuOnPeutRajouter)==0) # si on peut pas ajouter d'objet
		return [objetsPrecedantsMis]
	else # si on peut encore ajouter
		for i in objetsQuOnPeutRajouter
			copyplus = push!(deepcopy(objetsPrecedantsMis), i) 
			# on fait une copie avec un objet en plus que l'on peut ajouter et on fait l'appelle récursif avec un objet placé en plus et de la place libre en moins, on ajoute les résultats obtenus dans motifs
			append!(motifs, calculDesMotifsRec(copyplus, placeLibre-taillesDesObjets[i], taillesDesObjets))
		end
		return motifs
	end
end

# objetPrecedant est le dernier objet placé dans le bin
function onPeutAjouterQuoiCommeObjet(objetPrecedant::Int64, placeLibre::Int64, taillesDesObjets::Vector{Int64})
	if placeLibre==0
		return []
	end

	objetsQuOnPeutRajouter::Vector{Int64} = []
	for i in 1:length(taillesDesObjets)
		# si la taille de l'objet qu'on veut mettre est plus petite que le précédant et qu'il reste asse de place libre
		# alors on l'ajoute dans la liste des objets qu'on peut ajouter
		if ( (taillesDesObjets[objetPrecedant] >= taillesDesObjets[i]) && (taillesDesObjets[i] <= placeLibre) )
			push!(objetsQuOnPeutRajouter, i)
		end
	end
	return objetsQuOnPeutRajouter
end


function compteObjetIDansMotif(i::Int64, motif::Vector{Int64})
	nbRet::Int64 = 0;
	
	for k in 1:length(motif)
		if(motif[k] == i)
			nbRet = nbRet + 1;
		end
	end

	return nbRet
end


function scriptMono23()
	d::donnees1D = parser_data1D("Instances/1Dim/A/A4.dat")
	#d::donnees1D = parser_data1D("Instances/1Dim/A/jouet.dat")

	maFonctionQuiResoutTropBienLaModelisationIndirecte(d) # Première résolution à part pour que le code soit compilé

	indices::Vector{Int64} = [4,5,6,7,8,9,10,15,20]
	for i in indices
		println("Instance ",i)
		d = parser_data1D("Instances/1Dim/B/B$i.dat")
		@time maFonctionQuiResoutTropBienLaModelisationIndirecte(d)
		println()
	end
end


#-----------------------------------------------------------------------------------------------------
# 3.
function modelisationBidimensionnel(solverSelected::DataType, Motifs::Vector{Vector{Int64}}, nbObjDistincts::Int64, nbObjType::Vector{Int64})
    nbMotifs::Int64 = length(Motifs)
    
	# Déclaration du modèle
	m::Model = Model(solverSelected)
	
	# Déclaration des variables
	# nombre de bins correspondant au motif j
	@variable(m, x[1:nbMotifs] >= 0, integer = true)
    
	# Déclaration de la fonction objectif
	@objective(m, Min, sum(x[j] for j in 1:nbMotifs))
	
	# Déclaration des contraintes
	@constraint(m, cont[i=1:nbObjDistincts], sum(compteObjetIDansMotif(i,Motifs[j])*x[j] for j in 1:nbMotifs) >= nbObjType[i])
	
	# Valeur retournée
	return m
end

function maFonctionQuiResoutTropBienLaModelisationBidimensionnel(d::donnees2D)
	# Déclaration des données
	objetsAux::Vector{objet2D} = sort!(deepcopy(d.tab), by = x -> x.l, rev=true)
	println("Les objets: ",d.tab)
	
	# calcul du nombre d'objet total
	nbObjets::Int64 = sum(d.tab[i].nb for i in 1:length(d.tab))
	println("Nombre d'objets: ",nbObjets)
	# Taille des bins
	tailleDesBin::NamedTuple{(:L,:H),Tuple{Int64,Int64}} = (L=d.L, H=d.H)
	println("Taille des Bin: ",tailleDesBin)
	
	# tableau contenant les valeurs distinct des tailles des objets
	tailleObjetsDistinct::Vector{NamedTuple{(:l,:h),Tuple{Int64,Int64}}} = [(l=objetsAux[i].l, h=objetsAux[i].h) for i in 1:length(objetsAux)]
	println("Taille des Objets: ",tailleObjetsDistinct)
	# nombre d'objet de tailles différente
	nbObjDistincts::Int64 = d.nb
	# le nombre d'objet par type
	nbObjType::Vector{Int64} = [objetsAux[j].nb for j in 1:length(d.tab)]
	println("Nombre objets par type: ",nbObjType)

	
	# listes des motifs possible
	lesMotifs = donneLesMotifsBidim(nbObjDistincts, tailleObjetsDistinct, tailleDesBin)
	println("Il y a ",length(lesMotifs)," motifs")
	#println("Resultat des motifs: ", lesMotifs)
	
	# Création d'un modèle complété à partir des données
	m::Model = modelisationBidimensionnel(GLPK.Optimizer, lesMotifs, nbObjDistincts, nbObjType)
	
	#print(m)
	
	# Résolution
	optimize!(m)
	
	# Affichage des résultats (ici assez complet pour gérer certains cas d'"erreur")
	status = termination_status(m)

	if status == MOI.OPTIMAL
		println("Problème résolu à l'optimalité")

		println("z = ",objective_value(m)) # affichage de la valeur optimale
		println("x = ",value.(m[:x])) # affichage des valeurs du vecteur de variables issues du modèle


		for j in 1:length(lesMotifs)
			if (value(m[:x][j])!=0)
				println("nombre de bin correspondant au motif ",j,":",lesMotifs[j]," = ",value(m[:x][j]))
			end
		end
		println()

	elseif status == MOI.INFEASIBLE
		println("Problème non-borné")

	elseif status == MOI.INFEASIBLE_OR_UNBOUNDED
		println("Problème impossible")
	end
end

# lance la récursion en ajoutant un objet de chaques types par appelle récursif et récolte les motifs résultants
function donneLesMotifsBidim(nbObjDistincts::Int64, tailleObjetsDistinct::Vector{NamedTuple{(:l,:h),Tuple{Int64,Int64}}}, tailleDesBin::NamedTuple{(:L,:H),Tuple{Int64,Int64}})

	motifs::Vector{Vector{Int64}} = []
	
	for i in 1:nbObjDistincts
		append!(motifs, calculDesMotifsRecBidim([i], tailleDesBin, tailleObjetsDistinct))
	end
	
	return motifs
end

function calculDesMotifsRecBidim(objetsPrecedantsMis::Vector{Int64}, tailleBin::NamedTuple{(:L,:H),Tuple{Int64,Int64}}, taillesDesObjets::Vector{NamedTuple{(:l,:h),Tuple{Int64,Int64}}})
	# détermine si on peut encore ajouter un objet, le premier paramètre correspond au dernier élément ajouter a la liste des objets placés
	objetsQuOnPeutRajouter = onPeutAjouterQuoiCommeObjet(objetsPrecedantsMis, tailleBin, taillesDesObjets)
	
	motifs::Vector{Vector{Int64}} = []
	
	if (length(objetsQuOnPeutRajouter)==0) # si on peut pas ajouter d'objet
		return [objetsPrecedantsMis]
	else # si on peut encore ajouter
		for i in objetsQuOnPeutRajouter
			copyplus = push!(deepcopy(objetsPrecedantsMis), i) 
			# on fait une copie avec un objet en plus que l'on peut ajouter et on fait l'appelle récursif avec un objet placé en plus et de la place libre en moins, on ajoute les résultats obtenus dans motifs
			append!(motifs, calculDesMotifsRecBidim(copyplus, tailleBin, taillesDesObjets))
		end
		return motifs
	end
end


function modelMettreObjetFaisabilite(solverSelected::DataType, lesObjets::Vector{Int64}, tailleBin::NamedTuple{(:L,:H),Tuple{Int64,Int64}}, taillesDesObjets::Vector{NamedTuple{(:l,:h),Tuple{Int64,Int64}}})

	nbObjets::Int64 = length(lesObjets)
	L::Int64 = tailleBin.L
	H::Int64 = tailleBin.H
	M::Int64 = max(L, H)
	
	# Déclaration du modèle
	m::Model = Model(solverSelected)
	
	# Déclaration des variables
	# représente le point inférieur gauche de chaque objet
	@variable(m, x[1:nbObjets] >= 0)
	@variable(m, y[1:nbObjets] >= 0)
	# pour spécifier qu'il n'y a pas de chevauchement entre les paires d'objets
	@variable(m, b1[i=(1:nbObjets-1), j=(i+1:nbObjets)], binary = true)
	@variable(m, b2[i=(1:nbObjets-1), j=(i+1:nbObjets)], binary = true)
	@variable(m, b3[i=(1:nbObjets-1), j=(i+1:nbObjets)], binary = true)
	@variable(m, b4[i=(1:nbObjets-1), j=(i+1:nbObjets)], binary = true)
    
	
	# Déclaration des contraintes
	# objets doivent tenir dans le bin
	@constraint(m, Largeur[k=1:nbObjets], x[k]+taillesDesObjets[lesObjets[k]].l <= L)
	@constraint(m, Hauteur[k=1:nbObjets], y[k]+taillesDesObjets[lesObjets[k]].h <= H)
	
	
	# pas de chevauchements entre objets avec i<j
	@constraint(m, chevauchement1[i=(1:nbObjets-1), j=(i+1:nbObjets)], x[i]+taillesDesObjets[lesObjets[i]].l <= x[j]+M*(1-b1[i,j]))
	@constraint(m, chevauchement2[i=(1:nbObjets-1), j=(i+1:nbObjets)], x[j]+taillesDesObjets[lesObjets[j]].l <= x[i]+M*(1-b2[i,j]))
	@constraint(m, chevauchement3[i=(1:nbObjets-1), j=(i+1:nbObjets)], y[i]+taillesDesObjets[lesObjets[i]].h <= y[j]+M*(1-b3[i,j]))
	@constraint(m, chevauchement4[i=(1:nbObjets-1), j=(i+1:nbObjets)], y[j]+taillesDesObjets[lesObjets[j]].h <= y[i]+M*(1-b4[i,j]))

	
	
	@constraint(m, sommeb[i=(1:nbObjets-1), j=(i+1:nbObjets)], b1[i,j]+b2[i,j]+b3[i,j]+b4[i,j] >= 1)
	
	
	# Valeur retournée
	return m
end

function fonctionQuiRenvoieVraiSiOnPeutMettreLesObjets(lesObjets::Vector{Int64},	tailleBin::NamedTuple{(:L,:H),Tuple{Int64,Int64}}, taillesDesObjets::Vector{NamedTuple{(:l,:h),Tuple{Int64,Int64}}})

	# Création d'un modèle complété à partir des données
	m::Model = modelMettreObjetFaisabilite(GLPK.Optimizer, lesObjets, tailleBin, taillesDesObjets)
	
	#print(m)
	
	# Résolution
	optimize!(m)
	
	# resultat
	status = termination_status(m)
	#=
	print("Peut-on mettre les objets: ",lesObjets," ? ")
	if status == MOI.OPTIMAL
		println("OUI !")
		for i in 1:length(lesObjets)
			println("Objet ",i," (type ",lesObjets[i],") : (x=", value(m[:x][i])," ; y=",value(m[:y][i]),")")
		end
	else
		println("Et bah non...")
	end=#
	return (status == MOI.OPTIMAL)
end



# objetPrecedant est le dernier objet placé dans le bin
function onPeutAjouterQuoiCommeObjet(lesObjetsMis::Vector{Int64}, tailleBin::NamedTuple{(:L,:H),Tuple{Int64,Int64}}, taillesDesObjets::Vector{NamedTuple{(:l,:h),Tuple{Int64,Int64}}})

	objetsQuOnPeutRajouter::Vector{Int64} = []
	for i in 1:length(taillesDesObjets)
	
		if( i >= lesObjetsMis[length(lesObjetsMis)] )# pour éviter de répéter des mêmes motifs ordonnées différement
			copyplus = push!(deepcopy(lesObjetsMis), i)
			# On essaye de rajouter un objet dans l liste des objets déja mis et qui marche
			if ( fonctionQuiRenvoieVraiSiOnPeutMettreLesObjets(copyplus, tailleBin, taillesDesObjets) )
				push!(objetsQuOnPeutRajouter, i)
			end
		end
	end
	return objetsQuOnPeutRajouter
end


function scriptBidim3()
	d::donnees2D = parser_data2D("Instances/2Dim/A/A4.dat")
	#d::donnees2D = parser_data2D("Instances/2Dim/A/Exemple.dat")
	#d::donnees2D = parser_data2D("Instances/2Dim/B/B4.dat")

	maFonctionQuiResoutTropBienLaModelisationBidimensionnel(d) # Première résolution à part pour que le code soit compilé

	indices::Vector{Int64} = [4,5,6,7,8,9,10]
	for i in indices
		println("Instance ",i)
		d = parser_data2D("Instances/2Dim/A/A$i.dat")
		@time maFonctionQuiResoutTropBienLaModelisationBidimensionnel(d)
		println()
	end
	#=
	indices::Vector{Int64} = [4,7,8,10]
	for i in indices
		println("Instance ",i)
		d = parser_data2D("Instances/2Dim/B/B$i.dat")
		@time maFonctionQuiResoutTropBienLaModelisationBidimensionnel(d)
		println()
	end=#
end

