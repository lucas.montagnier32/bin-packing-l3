# Bin Packing - L3

### Contributeurs
- Erwan Boisteau-desdevices
- Lucas Montagnier

#### Le code est séparé en plusieurs parties:
- (lignes 1-108) Structures et parser.
- (lignes 110-168) Heuristique best-fit 1D.
- (lignes 170-273) Modélisation directe 1D.
- (lignes 275-358) Modélisation directe alternatif 1D.
- (lignes 360-512) Modélisation indirecte 1D.
- (lignes 514-735) Modélisation problème bi-dimensionnel.
